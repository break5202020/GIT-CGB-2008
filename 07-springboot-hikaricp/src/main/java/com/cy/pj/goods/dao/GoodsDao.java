package com.cy.pj.goods.dao;

import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Repository
public interface GoodsDao {
    List<Map<String,Object>> findGoods();
}
