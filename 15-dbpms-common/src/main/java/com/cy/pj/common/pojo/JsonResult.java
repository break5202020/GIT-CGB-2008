package com.cy.pj.common.pojo;

import lombok.Data;
/**借助此对象封装控制层响应到客户端的数据*/
@Data
public class JsonResult {
    /**响应状态码*/
    private Integer state=1;//1 表示ok,0表示error
    /**响应状态码对应的具体信息*/
    private String message="ok";
    /**响应数据(一般是查询时获取到的正确数据)*/
    private Object data;

    public JsonResult(){}
    public JsonResult(String message){
        this.message=message;
    }
    public JsonResult(Object data){
        this.data=data;
    }

    public JsonResult(Throwable e){
        this.state=0;
        this.message=e.getMessage();
    }
}
