package com.cy.pj.common.web;

import com.cy.pj.common.pojo.JsonResult;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RestControllerAdvice;
@Slf4j
@RestControllerAdvice
public class GlobalExceptionHandler {
    public JsonResult doHandleRuntimeException(RuntimeException e){
        e.printStackTrace();
        log.error("e.message {}",e.getMessage());
        return new JsonResult(e);
    }
}
