package com.cy.pj.common.web;

import com.cy.pj.common.exception.ServiceException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestControllerAdvice;

//@ControllerAdvice
//@ResponseBody
//@RestControllerAdvice = @ControllerAdvice + @ResponseBody
@RestControllerAdvice
public class GlobalExceptionHandler {
    @ExceptionHandler(ArithmeticException.class)
    public String doHandlerArithmeticController(ArithmeticException e) {
        return "这是一个HandlerArithmeticController异常："+e.getMessage();
    }

    @ExceptionHandler(ServiceException.class)
    public String doServiceException(ServiceException e) {
        return "这是一个Service异常："+e.getMessage();
    }

    @ExceptionHandler(RuntimeException.class)
    public String doRuntimeException(RuntimeException e) {
        return "这是一个Runtime异常："+e.getMessage();
    }

    @ExceptionHandler(Throwable.class)
    public String doHandlerThrowable(Throwable e) {
        return "系统维护中:  "+e.getMessage();
    }
}
