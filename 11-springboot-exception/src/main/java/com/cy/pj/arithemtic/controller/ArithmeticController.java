package com.cy.pj.arithemtic.controller;

import com.cy.pj.arithemtic.service.ArithmeticService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@ResponseBody
public class ArithmeticController {


//    @ExceptionHandler(ArithmeticException.class)
//    @ResponseBody
//    public String doHandlerArithmeticController(ArithmeticException e) {
//        return "这是一个异常："+e.getMessage();
//    }


    @RequestMapping("/doCompute/{n1}/{n2}")
    public String doCompute(@PathVariable Integer n1,@PathVariable Integer n2){
//        try {
            Integer result = n1 / n2;
            return "The Result is  " + result;
//        }catch(ArithmeticException e){
//            e.printStackTrace();
//            return "计算异常："+e.getMessage();
//        }
    }

    @Autowired
    private ArithmeticService arithmeticService;

    @RequestMapping("/doSum/{nums}")
    public String doSum(@PathVariable Integer...nums){
        return "求和结果为：  " + arithmeticService.sum(nums);

    }
}
