package com.cy.pj.arithemtic.service;


import com.cy.pj.common.exception.ServiceException;
import org.springframework.stereotype.Service;

@Service
public class ArithmeticService {

    public int sum(Integer...nums){
        if (nums==null||nums.length==0)
            throw new ServiceException("数据不能为空");
        int sum=0;
        for (int i = 0; i < nums.length; i++) {
            sum+=nums[i];
        }
        return sum;

    }
}
