package com.cy.pj.module.pojo;
/**基于此对象在服务端封装客户端的请求参数值*/
public class RequestParameter {
    private String name;

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return "RequestParameter{" +
                "name='" + name + '\'' +
                '}';
    }

    public void setName(String name) {
        this.name = name;
    }
}
