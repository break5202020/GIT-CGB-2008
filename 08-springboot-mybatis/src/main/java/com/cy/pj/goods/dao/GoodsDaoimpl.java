package com.cy.pj.goods.dao;

import com.cy.pj.goods.pojo.Goods;
import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;
@Repository//描述数据层实现类,用于交给spring管理
public class GoodsDaoimpl implements GoodsDao {//bean的名字goodsDaoImpl
    @Autowired
    private SqlSession sqlSession;

    @Override
    public List<Goods> findGoods(){
        //第一种方式：执行代码速度最快
        return sqlSession.selectList("com.cy.pj.goods.dao.GoodsDao.findGoods");

        //第二种方式：代码简洁
//        return sqlSession.getMapper(GoodsDao.class).findGoods();



    }
}
