package com.cy.pj.brand.service.impl;

import com.cy.pj.brand.dao.BrandDao;
import com.cy.pj.brand.pojo.Brand;
import com.cy.pj.brand.service.BrandService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BrandServiceImpl implements BrandService {
    private static final Logger log=
            LoggerFactory.getLogger(BrandServiceImpl.class);
    @Autowired
    private BrandDao brandDao;

    @Override
    public List<Brand> findBrands(String name){
        long t1=System.currentTimeMillis();
        List<Brand> list = brandDao.findBrands(name);
        long t2=System.currentTimeMillis();
        log.info("time:{}", t2-t1);
        return list;

    }


    @Override
    public int deleteById(Integer id){
        int rows = brandDao.deleteById(id);
        return rows;
    }

    @Override
    public int insertBrad(Brand brand) {
        int rows = brandDao.insertBrand(brand);
        return rows;
    }

    @Override
    public Brand findBrandById(Integer id) {
        Brand brand=brandDao.findBrandById(id);
        return brand;
    }



    //    @Override
//    public Brand findBrandById(Integer id) {
//        return brandDao.findBrandById(id);
//    }
//
    @Override
    public int doUpdate(Brand brand) {
        int rows = brandDao.doUpdate(brand);
        return rows;
    }
}
