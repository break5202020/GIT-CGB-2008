package com.cy.pj.brand.service;

import com.cy.pj.brand.pojo.Brand;

import java.util.List;

public interface BrandService {
    List<Brand> findBrands(String name);
    int deleteById(Integer id);
    int insertBrad(Brand brand);
//    Brand findBrandById(Integer id);
    Brand findBrandById(Integer id);
    int doUpdate(Brand brand);
}
