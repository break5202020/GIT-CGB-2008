package com.cy.pj.brand.dao;

import com.cy.pj.brand.pojo.Brand;
import org.apache.ibatis.annotations.*;


import java.util.List;

@Mapper
public interface BrandDao {
//    @Select("select * from tb_brand where name like concat('%',#{name},'%')")
    List<Brand> findBrands(String name);

    @Delete("delete from tb_brand where id=#{id}")
    int deleteById(Integer id);

    @Insert("insert into tb_brand values(null,#{name},#{remark},now())")
    int insertBrand(Brand brand);

    @Select("select * from tb_brand where id=#{id}")
    Brand findBrandById(Integer id);

//    @Update("update tb_brand set name=#{name},remark=#{remark} where id=#{id}")
    int doUpdate(Brand brand);




}
