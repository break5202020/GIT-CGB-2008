package com.cy.pj.brand.controoller;

import com.cy.pj.brand.pojo.Brand;
import com.cy.pj.brand.service.BrandService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
public class BrandControllerTests {
    @Autowired
    private BrandService brandService;

    @Test
    void testFindBrand(){
        List<Brand> list = brandService.findBrands(null);
        for (Brand b:list) {
            System.out.println(b);
        }
    }

    @Test
    void testUpdateBrand(Brand brand){
        int rows = brandService.doUpdate(brand);
        System.out.println(rows);
    }

    @Test
    void testDeleteBrand(Integer id){
        int rows = brandService.deleteById(id);
        System.out.println(rows);
    }
}
