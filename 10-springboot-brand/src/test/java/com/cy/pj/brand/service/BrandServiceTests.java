package com.cy.pj.brand.service;

import com.cy.pj.brand.pojo.Brand;
import com.cy.pj.brand.service.impl.BrandServiceImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.util.Assert;

import java.util.List;

@SpringBootTest
public class BrandServiceTests {
    @Autowired
    private BrandServiceImpl brandService;

    @Test
    void testFindBrand(){
        List<Brand> list = brandService.findBrands("小米");
        System.out.println("-----1-----");
        System.out.println(list.size());
        Assertions.assertEquals(1, list.size());//断言测试

        System.out.println("-----2-----");
        //第一种方式：
//        for (Brand b:list) {
//            System.out.println(b);
//        }

        //第二种方式：lambda 表达式
        list.forEach(brand -> System.out.println(brand));
        System.out.println("-----3-----");
        //第三种方式：方法引用
        list.forEach(System.out::println);
    }


    @Test
    void testDeleteById(Integer id){
        int rows = brandService.deleteById(id);
        System.out.println("删除："+rows);
    }

    @Test
    void testUpdateById(Brand brand){
        int rows = brandService.doUpdate(brand);
        System.out.println("修改记录："+rows);
    }
}
